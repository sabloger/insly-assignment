<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Policy Calculator</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <script src="assets/js/jquery.min.js"></script>
    <!--    <script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>-->
    <!--    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>-->

    <link rel="stylesheet" href="task-two.css">
</head>
<body>

<div class="wrapper">
    <?php if ($data['show_form']) { ?>
        <div class="container">
            <div class="jumbotron text-center">
                <h1>Policy Calculator</h1>
            </div>
            <div class="row">
                <div class="col-12">
                    <form action="" method="post" id="form">
                        <div class="form-group">
                            <label for="value">Estimated value of the car: (<span id="value-val"></span> EUR)</label>
                            <input type="number" class="form-control" id="value" name="value" min="100"
                                   max="100000" value="50000">
                        </div>
                        <div class="form-group">
                            <label for="tax">Tax percentage: (<span id="tax-val"></span>%)</label>
                            <input type="range" class="form-control-range" id="tax" name="tax" min="0" max="100"
                                   value="10">
                        </div>
                        <div class="form-group">
                            <label for="instalments">Number of instalments: (<span id="instalments-val"></span>)</label>
                            <input type="range" class="form-control-range" id="instalments" name="instalments" min="1"
                                   max="12" value="1">
                        </div>
                        <div class="btn-group center">
                            <button type="button" class="btn btn-primary" name="submit-form">Calculate</button>
                        </div>
                        <input type="hidden" name="datetime" id="dt">
                        <input type="hidden" name="form-submit" value="1">
                    </form>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if (key_exists('result', $data)) { ?>
        <div class="result">
            <table class="table">
                <thead>
                <tr>
                    <th></th>
                    <th>Policy</th>

                    <?php for ($i = 1; $i <= $data['result']['instalments']; $i++) { ?>
                        <th><?php echo $i; ?> instalment</th>
                    <?php } ?>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="text-left">Value</td>
                    <td><?php echo number_format($data['result']['value'], 2); ?></td>

                    <?php for ($i = 1; $i <= $data['result']['instalments']; $i++) { ?>
                        <td></td>
                    <?php } ?>
                </tr>
                <tr>
                    <td class="text-left">Base premium (<?php echo $data['result']['base_rate']; ?>%)</td>
                    <td><?php echo number_format($data['result']['base_price'], 2); ?></td>

                    <?php for ($i = 1; $i <= $data['result']['instalments']; $i++) { ?>
                        <td><?php echo $i < $data['result']['instalments'] ? number_format($data['result']['base_instalment'][0],2) : number_format($data['result']['base_instalment'][1],2); ?></td>
                    <?php } ?>
                </tr>
                <tr>
                    <td class="text-left">Commission (<?php echo $data['result']['commission_rate']; ?>%)</td>
                    <td><?php echo number_format($data['result']['commission'], 2); ?></td>

                    <?php for ($i = 1; $i <= $data['result']['instalments']; $i++) { ?>
                        <td><?php echo $i < $data['result']['instalments'] ? number_format($data['result']['commission_instalment'][0],2) : number_format($data['result']['commission_instalment'][1],2); ?></td>
                    <?php } ?>
                </tr>
                <tr>
                    <td class="text-left">Tax (<?php echo $data['result']['tax_rate']; ?>%)</td>
                    <td><?php echo number_format($data['result']['tax'], 2); ?></td>

                    <?php for ($i = 1; $i <= $data['result']['instalments']; $i++) { ?>
                        <td><?php echo $i < $data['result']['instalments'] ? number_format($data['result']['tax_instalment'][0],2) : number_format($data['result']['tax_instalment'][1],2); ?></td>
                    <?php } ?>
                </tr>
                <tr>
                    <td class="bold text-left">Total cost</td>
                    <td class="bold"><?php echo number_format($data['result']['total_price'], 2); ?></td>

                    <?php for ($i = 1; $i <= $data['result']['instalments']; $i++) { ?>
                        <td><?php echo $i < $data['result']['instalments'] ? number_format($data['result']['total_instalment'][0],2) : number_format($data['result']['total_instalment'][1],2); ?></td>
                    <?php } ?>
                </tr>
                </tbody>
            </table>
            <a href="">Calculate again</a>
        </div>
    <?php } ?>
</div>

<script>

    const onChange = $e => {
        const $this = $($e.target);
        const id = $this.attr('id');

        $(`#${id}-val`).text($this.val());
    };


    $(document).ready(function () {
        const $inputs = $('input');
        $inputs.each((e, l) => onChange({target: l}));
        $inputs.on('change', onChange);

        $('button[name=submit-form]').on('click', () => {
            const d = new Date();
            $('#dt').val(`${d.getDay()}-${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`);

            $('#form').submit();
        });
    });
</script>
</body>
</html>
