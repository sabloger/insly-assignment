# Write example query to get 1-person data in all languages

USE insly_assignment;

SELECT employees.*,

       en_info.introduction    AS introduction_en,
       en_info.work_experience AS work_experience_en,
       en_info.education_info  AS education_info_en,

       es_info.introduction    AS introduction_es,
       es_info.work_experience AS work_experience_es,
       es_info.education_info  AS education_info_es,

       fr_info.introduction    AS introduction_fr,
       fr_info.work_experience AS work_experience_fr,
       fr_info.education_info  AS education_info_fr
FROM employees
         LEFT JOIN employee_info AS en_info ON employees.id = en_info.employee_id AND en_info.info_language = 'en'
         LEFT JOIN employee_info AS es_info ON employees.id = es_info.employee_id AND es_info.info_language = 'es'
         LEFT JOIN employee_info AS fr_info ON employees.id = fr_info.employee_id AND fr_info.info_language = 'fr';

