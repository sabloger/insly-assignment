CREATE DATABASE insly_assignment CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

USE insly_assignment;

-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: insly_assignment
-- ------------------------------------------------------
-- Server version	5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employee_info`
--

DROP TABLE IF EXISTS `employee_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(20) NOT NULL,
  `info_language` enum('en','es','fr') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'en',
  `introduction` text COLLATE utf8mb4_unicode_ci,
  `work_experience` text COLLATE utf8mb4_unicode_ci,
  `education_info` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employee_info_employee_id_info_language_uindex` (`employee_id`,`info_language`),
  CONSTRAINT `employee_info_employees_id_fk` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee_info`
--

LOCK TABLES `employee_info` WRITE;
/*!40000 ALTER TABLE `employee_info` DISABLE KEYS */;
INSERT INTO `employee_info` VALUES (1,1,'en','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','The purpose of lorem ipsum is to create a natural looking block of text (sentence, paragraph, page, etc.) that doesn\'t distract from the layout. A practice not without controversy, laying out pages with meaningless filler text can be very useful when the focus is meant to be on design, not content.\n\nThe passage experienced a surge in popularity during the 1960s when Letraset used it on their dry-transfer sheets, and again during the 90s as desktop publishers bundled the text with their software. Today it\'s seen all around the web; on templates, websites, and stock designs. Use our generator to get your own, or read on for the authoritative history of lorem ipsum.','Until recently, the prevailing view assumed lorem ipsum was born as a nonsense text. “It\'s not Latin, though it looks like it, and it actually says nothing,” Before & After magazine answered a curious reader, “Its ‘words’ loosely approximate the frequency with which letters occur in English, which is why at a glance it looks pretty real.”\n\nAs Cicero would put it, “Um, not so fast.”\n\nThe placeholder text, beginning with the line “Lorem ipsum dolor sit amet, consectetur adipiscing elit”, looks like Latin because in its youth, centuries ago, it was Latin.'),(2,1,'es','Su equipo ha subido y bajado estas montañas analizando al detalle los 19 glaciares que quedan en el Pirineo. Son campañas duras en las que a veces hay que arrastrar equipo pesado, subir a crestas de más de 3.000 metros para grabar imágenes aéreas o pasar medio día sondeando el espesor de la nieve recién caída para calcular la extensión real del hielo que está debajo. También hace falta destreza para saber lanzar el dron al aire y aterrizarlo lejos de rocas y ríos, lo que puede echar a perder el trabajo de días, como explica Jesús Revuelto, del IPE. En otras ocasiones se usa un escáner láser mucho más preciso y pesado, pero que requiere subir en helicóptero, cuenta Esteban Alonso. Cuando cae la noche el equipo se mete en los sacos de dormir hasta el día siguiente.','“Las pérdidas de hielo que hemos visto son muy importantes”, explica Nacho López-Moreno, investigador del IPE y líder del proyecto. “El retroceso que vemos es especialmente preocupante porque nos aboca a una desaparición inminente de estos glaciares o a que queden apenas masas de hielo residual”, añade este geógrafo zaragozano que lleva estudiando estos glaciares más de 20 años.','Un glaciar se alimenta de nieve que mantiene el frío. La nieve caída un año se transforma al siguiente en hielo. Cuanta más nieve se conserve de año en año, más crecerá el glaciar. Ninguno de los 19 glaciares del Pirineo está ganando extensión, pues las altas temperaturas, que en las cumbres de alta montaña han subido el doble que la media global de la Tierra, no permiten que atesoren mucha de la nieve caída de un año para otro. Ninguno de ellos ha crecido en tamaño desde los años 70.\n\n'),(3,1,'fr','Alors que le couvre-feu avancé à 18 heures est entré en vigueur dans 15 départements français, la Grèce a annoncé une prolongation du confinement jusqu\'au 10 janvier. Si les vaccins commencent à être administrés en Europe, la crise du coronavirus semble loin d\'être terminée.\n\nAu Royaume-Uni, les hôpitaux ont commencé à recevoir des lots du vaccin Oxford / AstraZeneca, qui vient d\'être approuvé par les autorités sanitaires. Moins cher et plus facile à conserver que ses principaux concurrents, il commencera à être administré ce lundi.\n\nDans le pays, le niveau des contaminations au coronavirus reste élevé , si bien que les syndicats d\'enseignants réclament un report de la rentrée scolaire d\'au moins 15 jours, pour éviter une vague d\'infection chez les professeurs.','L\'Italie repousse l\'ouverture de ses stations de ski au 18 janvier\nL\'Italie a repoussé samedi l\'ouverture de ses stations de ski au 18 janvier, les autorités régionales ayant demandé davantage de temps, estimant que les conditions n\'étaient pas réunies pour une ouverture initialement prévue le 7.\n\n\"Il s\'avère que les conditions ne sont pas réunies pour une ouverture des installations le 7 janvier\", ont estimé les autorités régionales, citées dans le décret signé samedi par le ministre italien de la Santé, Roberto Speranza, repoussant la date d\'ouverture des stations.\n\nLe début de la vaccination en Italie dimanche dernier est intervenu après que le pays a reconfiné, juste avant Noël, pour éviter une explosion des contaminations pendant les fêtes et repousser la spectre d\'une troisième vague, redoutée par les soignants.\n\nLe pays, qui a enregistré samedi 9 166 nouveau cas et 364 morts, espère poursuivre en janvier la campagne de vaccination avec 470 000 injections prévues chaque semaine.','Le couvre-feu avancé à 18 heures est entré en vigueur en France\nPour l\'heure, ce sont six millions d\'habitants de l\'Est de la France qui voient le couvre-feu avancé de 20H00 à 18H00 depuis samedi, en raison d\'un regain inquiétant de l\'épidémie. Les Alpes Maritimes et les Hautes-Alpes sont aussi concernés.\n\nGlobalement respecté dans le pays, le couvre-feu a été cependant défié dans l\'ouest de la France, dans la petite commune bretonne de Lieuron, où une rave party sauvage a réuni illégalement 2 500 \"teufeurs\" arrivés jeudi soir des quatre coins du pays mais aussi de l\'étranger.\n\nLes derniers chiffres de Santé Publique France vendredi montrent une légère hausse du nombre de patients hospitalisés et en réanimation, après une baisse continue ces derniers jours. Avec 157 nouveaux décès en 24 heures, le Covid a causé la mort de 65.921 personnes depuis le début de l\'épidémie.');
/*!40000 ALTER TABLE `employee_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth_date` date DEFAULT NULL,
  `id_code_ssn` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_employee` tinyint(1) NOT NULL DEFAULT '1',
  `email` varchar(320) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employees_users_created_by_fk` (`created_by`),
  KEY `employees_users_updated_by_fk` (`updated_by`),
  CONSTRAINT `employees_users_created_by_fk` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `employees_users_updated_by_fk` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES (1,'Mohammad Saeed Falsafin','1992-09-05','123-45-6789',1,'sabloger@gmail.com','989355285459','Unit 7, No. 11, Kargary alley, Bozorgmehr st., Isfahan, Iran.','2021-01-03 09:44:43',1,'2021-01-03 10:14:34',1);
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'John','Doe');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;