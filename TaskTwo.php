<?php

/**
 * Class TaskTwo
 *
 * Task Name:
 * Calculator
 *
 * Task Description:
 *
 */
class TaskTwo
{

    const NORMAL_BASE_RATE = 0.11;
    const FRIDAY_BASE_RATE = 0.13;

    const COMMISSION_RATE = 0.17;

    public function __invoke()
    {
        if (isset($_POST['form-submit'])) {
            $this->controller();
        } else {
            $this->view(['show_form' => true]);
        }
    }

    public function controller()
    {
        $value = $_POST['value'];
        $taxRate = $_POST['tax'] / 100;
        $instalments = $_POST['instalments'];

        // Base price of policy is 11% from entered car value, except every Friday 15-20 o’clock (user time) when it is 13%
        $basePrice = round($value * self::NORMAL_BASE_RATE, 2);
        $baseRate = self::NORMAL_BASE_RATE * 100;

        //Expected Format: 4-18:8:0 day-hour:min:sec
        $date = explode('-', $_POST['datetime']);
        $day = $date[0];
        if ($day == 5) { // 5 is JS Friday number
            list($hour, $min, $sec) = explode(':', $date[1]);
            if (($hour >= 15 && $hour <= 19) || ($hour == 20 && $min == 0 && $sec == 0)) {
                $basePrice = round($value * self::FRIDAY_BASE_RATE, 2);
                $baseRate = self::FRIDAY_BASE_RATE * 100;
            }
        }

        // Commission is added to base price (17%)
        $commission = round($basePrice * self::COMMISSION_RATE, 2);

        // Tax is added to base price (user entered)
        $tax = round($basePrice * $taxRate, 2);

        // Calculate Total
        $totalPrice = $basePrice + $commission + $tax;

        // Calculate different payments separately (if number of payments are larger than 1)
        $baseInstalment = null;
        $commissionInstalment = null;
        $taxInstalment = null;
        $totalInstalment = null;
        if ($instalments > 1) {
            $baseInstalment = $this->calculateInstalments($basePrice, $instalments);
            $commissionInstalment = $this->calculateInstalments($commission, $instalments);
            $taxInstalment = $this->calculateInstalments($tax, $instalments);
            $totalInstalment = $this->calculateInstalments($totalPrice, $instalments);
        }

        $this->view([
            'show_form' => false,
            'result' => [
                'value' => $value,
                'base_price' => $basePrice,
                'base_rate' => $baseRate,
                'commission' => $commission,
                'commission_rate' => self::COMMISSION_RATE * 100,
                'tax' => $tax,
                'tax_rate' => $taxRate * 100,
                'total_price'=>$totalPrice,
                'instalments' => $instalments > 1 ? $instalments : 0,
                'base_instalment' => $baseInstalment,
                'commission_instalment' => $commissionInstalment,
                'tax_instalment' => $taxInstalment,
                'total_instalment' => $totalInstalment,
            ],
        ]);
    }

    private function calculateInstalments($price, $instalments)
    {
        $instalment = round($price / $instalments, 2);
        $lastInstalment = $instalment;
        if ($instalment * $instalments != $price) {
            $lastInstalment = $price - ($instalment * ($instalments - 1));
        }
        return [$instalment, $lastInstalment];
    }

    private function view($data = [])
    {
        eval('?>' . file_get_contents('task-two.html.php') . '<?php;');
    }
}


$taskObj = new TaskTwo;

$taskObj();